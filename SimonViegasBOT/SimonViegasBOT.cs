﻿using System;
using System.Collections.Generic;
using System.Text;
using Robocode;
using Robocode.Util;
using System.Drawing;

namespace SOLUTIS.SimonViegasBOT
{
    public class SimonViegasBOT : AdvancedRobot
    {
        //quanto virar a arma para ir em direção ao adversário que está sendo rastreado
        double gunTurnAmt = 10;

        //contador de loops
        int countLoops = 0;

        //tiros errados
        int countFails = 0;

        //fator de potência para o tiro (economizar quando está errando muito)
        double powerFactor = 1;

        public override void Run()
        {
            //Faz a arma girar independentemente do tanque
            IsAdjustGunForRobotTurn = true;

            //configuração de cores
            Color solutisColor = Color.FromArgb(0, 149, 235);
            Color bodyColor = solutisColor;
            Color gunColor = Color.White;
            Color radarColor = Color.Black;
            Color bulletColor = Color.Red;
            Color scanArcColor = Color.Yellow;

            //define a cor do tanque
            SetColors(
                bodyColor,
                gunColor,
                radarColor,
                bulletColor,
                scanArcColor
                );

            for(;;)
            {
                //marca para girar a arma em direção ao oponente que está sendo rastreado
                SetTurnGunRight(gunTurnAmt);

                //marca para girar o tanque para direita
                SetTurnRight(30);

                //marca para andar para frente
                SetAhead(200);

                countLoops++;

                //se após 5 loops não encontrar o sacaninha, gira o radar um pouco para esquerda
                if (countLoops > 5)
                {
                    gunTurnAmt = -10;
                }

                //se após 10 loops não encontrar o sacaninha, gira o radar um pouco para direita
                if (countLoops > 10)
                {
                    gunTurnAmt = 10;
                }

                Execute();
            }
        }

        public override void OnScannedRobot(ScannedRobotEvent evnt)
        {
            //calcula quanto virar para apontar para o adversário que está sendo rastreado
            gunTurnAmt = Utils.NormalRelativeAngleDegrees(evnt.Bearing + (Heading - RadarHeading));

            //se errou mais de 2 tiros
            if (countFails > 2)
            {
                //Console.WriteLine("FORÇA REDUZIDA");
                
                //40% da força
                powerFactor = .4; 
            }

            //Dá um tiro de acordo com a do inimigo
            if (evnt.Energy > 16)
            {
                Fire(3 * powerFactor);
            }
            else if (evnt.Energy > 10)
            {
                Fire(2 * powerFactor);
            }
            else if (evnt.Energy > 4)
            {
                Fire(1);
            }
            else if (evnt.Energy > 2)
            {
                Fire(.5);
            }
            else
            {
                Fire(.1);
            }
        }

        public override void OnHitRobot(HitRobotEvent evnt)
        {
            //Se o oponente está na frente do tanque, afaste-se um pouco.
            if (evnt.Bearing > -90 && evnt.Bearing < 90)
            {
                SetBack(100);
            } //Se ele estivessar atrás, anda um pouco para frente
            else
            {
                SetAhead(50);
            }
            Execute();
        }

        public override void OnBulletMissed(BulletMissedEvent evnt)
        {
            //Console.WriteLine("Errouuuu");
            countFails++;
        }

        public override void OnBulletHit(BulletHitEvent e)
        {
            //Console.WriteLine("Acertou, miserávi");
            countFails = 0;
            powerFactor = 1;
        }

        public override void OnHitWall(HitWallEvent evnt)
        {
            //Se a parede está na frente do tanque, afaste-se um pouco.
            if (evnt.Bearing > -90 && evnt.Bearing < 90)
            {
                SetBack(100);
                SetTurnRight(-10);
            } //Se ela estivessar atrás, anda um pouco para frente
            else
            {
                SetAhead(50);
                SetTurnRight(-10);
            }
        }
    }
}