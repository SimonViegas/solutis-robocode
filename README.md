# Simon Viegas BOT

Tanque maroto em Robocode

## 🚀 Pontos Fortes
```
- Possui travamento de mira num inimigo;
- Ataca e movimenta ao mesmo tempo;
- Movimentação circular, mas com algumas variáveis por eventos;
- Sistemas de economia de energia (poder dos tiros).
```

## 😏 Pontos Fracos
```
- Não desvia de paredes;
- Não desvia de balas;
```

## ✒️ Autor
Simon Viegas
